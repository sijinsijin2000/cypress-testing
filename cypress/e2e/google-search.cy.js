// describe('template spec', () => {
//   it('passes', () => {
//     cy.visit('https://www.google.com')
//     cy.get('#APjFqb').type('Automation Testing')
//     cy.get('input[value="Google Search"]').first().click()

//     // cy.get('.FPdoLc > center > .gNO89b').submit()
//   })
// })

describe('Google Search', () => {
  it('loads search page', () => {
    cy.visit('https://www.google.com')
    cy.get('.gLFyf').type('Automation Testing')
    cy.get('input[value="Google Search"]').first().click()
  })
})
